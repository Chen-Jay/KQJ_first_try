<?php
function renewMachine()
{
	$order = array(
		'status' => 1,
		'info' => 'ok',
		'data' => array(
			'id'=>'0',
			'do'=>'update',
			'data'=>'config',				 
			'name'=>'5班实验机',			 //设备名称
			'company'=>'华南理工大学软件学院',	//公司名称
			'companyid'=>0,					 //公司id
			'max'=>3000,					 //设备最大容纳员工数量
			'function'=>65535,				 //功能，65535 表示全功能开放 1 -》密码功能 2-》拍照功能 16-》门禁
			'delay'=>10,						 //表示无新数据请求服务器间隔，单位秒	
			'errdelay'=>10,					 //当请求服务器发生错误时，再次发送请求的间隔，单位秒
			'timezone'=>'GMT+08:00',		 //设备时区设定
			'encrypt'=>0,                    //预留字段
			'expired'=>'2015-12-10 12:10:10' //预留字段	
			)
		);
	return json_encode($order);	
}
