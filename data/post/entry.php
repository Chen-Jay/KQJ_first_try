<?php
/**编辑日志文件格式**/
error_reporting(E_ALL);
require_once ('common_function.php');
define('POST_LOG', '/tmp/post.log', FILE_APPEND);
file_put_contents(POST_LOG, date('Y/m/d H:i:s')."\n");

/**由于发过来的信息是json格式的，不能够直接被服务器解析，因此，我们获得整条请求体部分，储存到$raw中*/
$raw = file_get_contents('php://input');    
file_put_contents(POST_LOG, $raw."\n", FILE_APPEND);
//var_dump($raw);
dataHandler($raw);

