<?php
class DataHandler
{
    static public function getMassages();
    static public function handleData($data);
    static private function verifyData($data);
    static private function checkdate($json);

    static private function handleReturn($json);

}

